import React from "react";
import "./Chat.css";

import ChatHeader from "./ChatHeader";
import MessageList from "./MessageList";
import MessageInput from "./MessageInput";

class Chat extends React.Component {
  state = { messages: [], userName: "Dave", editing: null }; //userName: this is current logged in user

  constructor(props) {
    super(props);
    this.messageListRef = React.createRef();
  }

  componentDidMount() {
    fetch("https://api.myjson.com/bins/1hiqin")
      .then(response => response.json())
      .then(messages => this.setState({ messages }));
  }

  componentDidUpdate(prevProps, prevState) {
    // scroll down only if number of messages changed
    if (prevState.messages.length !== this.state.messages.length) {
      this.scrollDown();
    }
  }

  scrollDown() {
    // cannot rewrite using refs for some unknown reason
    const messageListElem = document.querySelector(".MessageList");
    if (messageListElem) {
      messageListElem.scrollTop = messageListElem.scrollHeight;
    }
  }

  getUniqueId() {
    const maxId = this.state.messages
      .map(item => item.id)
      .sort((b, a) => a - b)[0];
    return maxId + 1;
  }

  onMessageSend(messageText) {
    const newMessage = {
      user: this.state.userName,
      created_at: new Date().toString(),
      avatar: "",
      message: messageText,
      id: this.getUniqueId()
    };
    this.setState({
      messages: [...this.state.messages, newMessage]
    });
    this.scrollDown();
  }

  onEdit(id) {
    this.setState({ editing: id });
    this.messageEditText = this.state.messages.find(
      item => item.id === id
    ).message;
  }

  onDelete(id) {
    const newMessages = [...this.state.messages].filter(item => item.id !== id);
    this.setState({ messages: newMessages });
  }

  onLike(id) {
    const newMessages = [...this.state.messages];
    const index = newMessages.findIndex(item => item.id === id);
    newMessages[index].liked = !newMessages[index].liked;
    this.setState({ messages: newMessages });
  }

  onMessageEditSend(messageText) {
    console.log("On message edited send text: " + messageText);
    const newMessages = [...this.state.messages];
    const index = newMessages.findIndex(item => item.id === this.state.editing);
    newMessages[index].message = messageText;
    this.messageEditText = "";
    this.setState({ editing: null, messages: newMessages });
  }

  render() {
    if (this.state.messages.length === 0) {
      return (
        <div className="loading">
          <h2 className="loadingText">Loading...</h2>
          <div className="spinner" />
        </div>
      );
    }
    return (
      <main className="Chat">
        <ChatHeader
          messages={this.state.messages}
          userName={this.state.userName}
        />
        <MessageList
          messages={this.state.messages}
          userName={this.state.userName}
          ref={this.messageListRef}
          onEdit={id => this.onEdit(id)}
          onDelete={id => this.onDelete(id)}
          onLike={id => this.onLike(id)}
        />
        <MessageInput
          onMessageSend={messageText => {
            this.state.editing
              ? this.onMessageEditSend(messageText)
              : this.onMessageSend(messageText);
          }}
          messageEditText={this.messageEditText}
        />
      </main>
    );
  }
}

export default Chat;
