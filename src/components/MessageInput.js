import React from "react";

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { message: "" };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ message: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.onMessageSend(this.state.message);
    this.setState({ message: "" });
  }

  componentWillReceiveProps(nextProps) {
    // You don't have to do this check first, but it can help prevent an unneeded render
    if (nextProps.messageEditText !== this.state.messageEditText) {
      this.setState({ message: nextProps.messageEditText });
    }
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit} className="MessageInput">
        <input
          type="text"
          className="messageInputElement"
          onChange={this.handleChange}
          value={this.state.message}
        />
        <button className="messageSendButton">
          <i className="material-icons">send</i>
        </button>
      </form>
    );
  }
}

export default MessageInput;
