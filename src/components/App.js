import React from "react";
import "./App.css";

import Header from "./Header";
import Chat from "./Chat";

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Chat />
      </div>
    );
  }
}

export default App;
