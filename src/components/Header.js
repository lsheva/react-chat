import React from "react";
import logo from "../logo.svg";

function Header() {
  return (
    <header className="Header">
      <div className="logo">
        <img src={logo} alt="Reactive chat logo" />
      </div>
      <h1 className="siteName">
        React<span>ive</span> Chat
      </h1>
    </header>
  );
}

export default Header;
