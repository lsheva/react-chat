import React from "react";
import Message from "./Message";

class MessageList extends React.Component {
  renderMessages() {
    const messages = this.props.messages.map(messageObj => {
      return (
        <Message
          messageObj={messageObj}
          private={messageObj.user === this.props.userName}
          key={messageObj.id}
          onEdit={this.props.onEdit}
          onDelete={this.props.onDelete}
          onLike={this.props.onLike}
        />
      );
    });
    const separatedMessages = this.addSeparators(messages);
    return separatedMessages;
  }
  addSeparators(messages) {
    const datesArr = messages
      .map(item => new Date(item.props.messageObj.created_at).toDateString())
      .filter(this.onlyUnique);
    const messagesWithSeparators = datesArr.reduce((acc, item) => {
      acc.push(this.renderSeparator(item));
      acc.push(
        messages.filter(
          value =>
            new Date(value.props.messageObj.created_at).toDateString() === item
        )
      );
      return acc;
    }, []);
    return messagesWithSeparators;
  }
  renderSeparator(date) {
    return (
      <div className="separator" key={date}>
        {date}
      </div>
    );
  }

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
  render() {
    return <div className="MessageList">{this.renderMessages()}</div>;
  }
}

export default MessageList;
