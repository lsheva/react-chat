import React from "react";

const renderMessage = (messageObj, onLike) => {
  return (
    <div className="messageWrapper">
      <div className="Message">
        <div className="avatar">
          <img src={messageObj.avatar} alt={`${messageObj.user}'s avatar`} />
        </div>
        <div className="messageContent">
          <h3 className="userName">{messageObj.user}</h3>
          <p className="messageText">{messageObj.message}</p>
          <div className="messageInfo">
            <div className="messageDate">
              {new Date(messageObj.created_at).toLocaleTimeString()}
            </div>
          </div>
        </div>
      </div>
      <div className="like">
        <div
          className={`like-button ${messageObj.liked ? "is-active" : ""}`}
          onClick={() => onLike(messageObj.id)}
        >
          <i className="material-icons not-liked bouncy">favorite_border</i>
          <i className="material-icons is-liked bouncy">favorite</i>
          <span className="like-overlay" />
        </div>
      </div>
    </div>
  );
};

const renderPrivateMessage = (messageObj, onEdit, onDelete) => {
  return (
    <div className="Message private">
      <div className="messageContent">
        <p className="messageText">{messageObj.message}</p>
        <div className="messageInfo">
          <div className="messageDate">
            {new Date(messageObj.created_at).toLocaleTimeString()}
          </div>
          <div className="messageEdit" onClick={() => onEdit(messageObj.id)}>
            Edit
          </div>
          <div
            className="messageDelete"
            onClick={() => onDelete(messageObj.id)}
          >
            Delete
          </div>
        </div>
      </div>
    </div>
  );
};

const Message = props => {
  if (props.private) {
    return renderPrivateMessage(props.messageObj, props.onEdit, props.onDelete);
  }
  return renderMessage(props.messageObj, props.onLike);
};

export default Message;
