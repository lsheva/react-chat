import React from "react";

class ChatHeader extends React.Component {
  getParticipantsCount() {
    // reduce array to only unique user names and return its length
    return this.props.messages.reduce(
      (unique, item) =>
        unique.includes(item.user) ? unique : [...unique, item.user],
      []
    ).length;
  }

  getMessagesCount() {
    return this.props.messages.length;
  }

  getLastMessageTime() {
    const lastMessage = [...this.props.messages].sort(
      (b, a) => new Date(a.created_at) - new Date(b.created_at)
    )[0];
    return lastMessage
      ? new Date(lastMessage.created_at).toLocaleString()
      : null;
  }

  render() {
    return (
      <div className="ChatHeader">
        <div className="chatName">{`${this.props.userName}'s chat`}</div>
        <div className="participantsCount">
          {this.getParticipantsCount()} participants
        </div>
        <div className="messagesCount">{this.getMessagesCount()} messages</div>
        <div className="lastMessageTime">
          last message at {this.getLastMessageTime()}
        </div>
      </div>
    );
  }
}

export default ChatHeader;
